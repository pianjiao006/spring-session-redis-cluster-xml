#spring-session-redis-cluster-xml是基于xml配置实现的
1. 先搭建redis cluster，可以参考搭建集群https://www.cnblogs.com/kangoroo/p/7657616.html
2. 文本搭建好redis集群，创建maven项目，引入spring-session相关的包
3. 部署到不同的tomcat或者jetty，启动访问：http://ip:port/spring-session-redis-cluster-xml/spring/session/getSession，可以看到效果！
4. 获取结果为sessionId
特别说明，统一浏览器访问才是相同的sessionId！！